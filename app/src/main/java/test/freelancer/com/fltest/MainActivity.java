package test.freelancer.com.fltest;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import test.freelancer.com.fltest.utils.PrefsManager;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PrefsManager.init(this);
        setContentView(R.layout.activity_main);
    }

}
